# Welcome to Barduino Docs

This page is the documentation for the Barduino 4 family boards, designed by Fablab Barcelona. 

![Barduino 4.2](images/landing.jpg)

Here you will find the necessary information on how to use your Barduino 4.0 boards. 

- Learn about the history of the Barduino [here](history/index.md)

- Follow this instructions to install everything in Arduino [here](GettingStarted/arduinoIDE.md)

- You can always check the pinout [here](GettingStarted/pinout.md)

- There are some examples available [here](files/ArduinoExamples.zip)

For any question feel free to reach out to josep@fablabbcn.org and feel free to contribut to this page :smile:
