# Barduino, what is that?

Barduino was born in 2011 as a development board for [Fabacademy](https://fabacademy.org/) students, created by Luciano Betoldi. It was based on the ATMEGA328P and programmed by ISP together with the [FabISP](https://fab.cba.mit.edu/content/archive/projects/fabisp/) and was used by many students in the following years as a main reference and board for their final projects. The name came from Barcelona and Arduino, as it was created in Barcelona, and it was a "fabbable" version of an Arduino UNO. 

![Barduino 1](../images/Barduino1.png)

After some years, in 2020, the [Barduino 2.0](https://gitlab.com/fablabbcn-projects/electronics/barduino) was born again in Barcelona, but this time it was created by Eduardo Chamorro with the help of Oscar Gonzalez and Josep Martí. The idea was to renovate the initial idea, keeping it as a development board for Fabacademy students and other Fablab users, but changing the microcontroller to an ESP32, which was more commonly used at the time, allowing the students to connect their boards through Wi-Fi or Bluetooth. This board was designed and produced just before the COVID-19 lockdown for students to be able to program their own boards at home, and it was a great team effort. 

![Barduino 2.0](https://gitlab.com/fablabbcn-projects/electronics/barduino/-/raw/master/img/0.jpeg)

The first version, Barduino 2.0, was programmed through a FTDI cable/board. But then the following versions 2.1 and 2.2 included an FT230XS FTDI chip and a microUSB connection to program the board. These improvements reduced the amount of hardware needed, as an FTDI cable/adapter was not easily found anywhere, but also made the production process and soldering a bit more complicated. 

![Barduino 2.2](https://gitlab.com/fablabbcn-projects/electronics/barduino/-/raw/master/img/Barduino2.2.jpg)

In 2022, the Barduino 3.0 was created. The goal for this board was to make the fabrication process simpler and more versatile, replacing the FT230XS chip with an ATSAMD11C14A based on the [Bridge Serial D11C](https://gitlab.fabcloud.org/pub/programmers/programmer-serial-d11c) created by Quentin Bolsee. This allowed a SAMD11 to act as a Serial bridge (previously programmed by the on-board SWD programming pins) to program the ESP32 and allow the Serial communication. It also exposed the SAMD11C pins to be able to program it independently, so it was a dual microcontroller development board. 

!!! note
    For the Barduino 3.0 the ESP32-WROOM-DA module was used because it was the one available at that time during the global chip shortage. 

![Barduino 3.0](https://fabacademy.org/2023/labs/barcelona/students/manuela-reyes/assets/img/Assignments/04_embeded/01-coverembeded.jpg)

After that experience, we decided to create a new version of the Barduino that includes more things than just exposing the pins, with the idea of having a device for learning electronics and embedded programming without the need to debug a bad connection on a breadboard. And that is how the Barduino 4.0 was born. It uses an ESP32-S3 that allows direct USB programming (with a USB C connector) and it has different peripherals like an LED, a Neopixel, a Buzzer, a Light sensor, a Temperature sensor, a Button and many capacitive touch pins, while still exposing most of the pins, making it breadboard compatible and perfect for learning electronics. 

![Barduino 4.2](../images/landing.jpg)